var express = require('express'),
    mongoose = require('mongoose'),
    parser = require('body-parser');
    cors = require('cors');

var Patient = require('./api/model/Patient');
var Overview = require('./api/model/Overview');

var app = express(),
    port = process.env.PORT || 3000;


mongoose.Promise = global.Promise;
mongoose.connect('mongodb://localhost/TBDashboardDB');

app.use(cors());
app.use(parser.urlencoded({ extended: true }));
app.use(parser.json());


var routes = require('./api/route/routes'); 

routes(app); 
app.listen(port);

console.log(`TB-Dashboard Server is running on port ${port}`);

 





