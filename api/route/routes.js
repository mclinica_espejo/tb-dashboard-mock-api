'use strict';

module.exports = function(app) {
  var patientController = require('../controller/patient.controller.js');
  var overviewController = require('../controller/overview.controller.js');

  // Patients
  app.route('/patients')
    .get(patientController.getPatients)
    .post(patientController.createPatient);

  app.route('/patients/:patientId')
    .get(patientController.getPatient)
    .put(patientController.updatePatient)
    .delete(patientController.deletePatient);

  // Overview
  app.route('/overview')
    .get(overviewController.getOverview)
    .post(overviewController.createOverview)

  app.route('/overview/:overviewId')
    .delete(overviewController.deleteOverview);
};
