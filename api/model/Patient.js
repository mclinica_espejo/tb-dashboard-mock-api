'use strict';
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var PatientSchema
 = new Schema({
  _id: Number,
  date_of_birth: Date,
  reference_code: String,
  facility: String,
  mobile_phone: String,
  sex: String,
  date_of_referral: Date,
  date_of_visit: Date,
});

module.exports = mongoose.model('Patients', PatientSchema);