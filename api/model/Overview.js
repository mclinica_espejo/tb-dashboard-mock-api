'use strict';
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var ReferralSchema = new Schema({
    total: Number,
    pending: Number,
    declined: Number
})

var VisitSchema = new Schema({
    validated: Number,
    pending: Number,
    confirmed: Number
})

var SurveySchema = new Schema({
    completed: Number,
    pending: Number
})

var OverviewSchema
 = new Schema({
  referral: ReferralSchema,
  visit: VisitSchema,
  survey: SurveySchema
});

module.exports = mongoose.model('Overview', OverviewSchema);