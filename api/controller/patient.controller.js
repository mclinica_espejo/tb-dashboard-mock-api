'use strict';

var mongoose = require('mongoose'),
  Patient = mongoose.model('Patients');

exports.getPatients = function(req, res) {
  Patient.find({}, function(err, patient) {
    if (err)
      res.send(err);
    res.json(patient);
  });
};

exports.createPatient = function(req, res) {
  var newPatient = new Patient(req.body)
      newPatient.save( function(err, patient) {
        if (err)
        res.send(err);
        res.json(patient);
      });
};

exports.getPatient = function(req, res) {
    Patient.find({_id: req.params.patientId}, function(err, patient) {
      if (err)
        res.send(err);
      res.json(patient);
    });
};

exports.updatePatient = function(req, res) {
    Patient.findOneAndUpdate({_id: req.params.patientId}, req.body, {new: true}, function(err, patient) {
      if (err)
        res.send(err);
      res.json(patient);
    });
};

exports.deletePatient = function(req, res) {
    Patient.remove({_id: req.params.patientId}, function(err, patient) {
      if (err)
        res.send(err);
      res.json(patient);
    });
};
