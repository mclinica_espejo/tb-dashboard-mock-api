'use strict';

var mongoose = require('mongoose'),
  Overview = mongoose.model('Overview');

exports.getOverview = function(req, res) {
  Overview.find({}, function(err, stats) {
    if (err)
      res.send(err);
    res.json(stats);
  });
};

exports.createOverview = function(req, res) {
    var newOverview = new Overview(req.body)
        newOverview.save( function(err, stats) {
          if (err)
          res.send(err);
          res.json(stats);
        });
  };

  exports.deleteOverview = function(req, res) {
    Overview.remove({_id: req.params.overviewId}, function(err, stats) {
      if (err)
        res.send(err);
      res.json(stats);
    });
  };

